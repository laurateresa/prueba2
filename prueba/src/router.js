import Router from 'vue-router'
import home from './domain/home/routes'
import game from './domain/game/routes'

const router = new Router({
  mode: 'history',
  routes: [
      home,
      game,
    { path: '*', redirect: home.path }
  ]
})


export default router
