const Home = () => import('./views/Home.vue')

export default {
  path: '/home',
  name: 'home',
  component: Home
}
