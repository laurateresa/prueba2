const Game = () => import('./views/Game.vue')

export default {
  path: '/game/:name',
  name: 'game',
  props: true,
  component: Game
}
