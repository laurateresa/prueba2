import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Router from 'vue-router'
import Vuex from 'vuex'

Vue.config.productionTip = false
Vue.use(Router)
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    users: [],
    dataSelected: []
  },
  mutations: {
    setUsers(users) {
      Vue.set(this, 'users', users)
    },
    setDataSelected(dataSelected) {
      Vue.set(this, 'dataSelected', dataSelected)
    }
  }
})

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
